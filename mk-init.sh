#!/bin/sh

set -e

if [ ! -d netsim ]; then

    ncs-netsim --dir netsim  \
               create-device service-node/packages/cisco-ios ios_0\
               create-device service-node/packages/cisco-ios ios_1\

fi

dir=service-node
mkdir ${dir}/ncs-cdb;
mkdir ${dir}/logs
mkdir ${dir}/state

echo '<config xmlns="http://tail-f.com/ns/config/1.0">' > \
     service-node/ncs-cdb/netsim_devices_init.xml

ncs-netsim ncs-xml-init ios_0 >> service-node/ncs-cdb/netsim_devices_init.xml
ncs-netsim ncs-xml-init ios_1 >> service-node/ncs-cdb/netsim_devices_init.xml

echo '</config>' >> \
     service-node/ncs-cdb/netsim_devices_init.xml
