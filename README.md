# NSO Service Reconciliation #

This repository provides a basic infrastructure for NSO service reconciliation along with sample services that 'use'
the infrastructure for reconciliation.  This code is intended to be adapted to specific NSO reconciliation scenarios
with actual services.  

### Code Structure ###

This code consists of 3 NSO packages and would be placed in an NSO project `service-node/packages` directory.  The `reconcile` package contains the `BaseHandler.py` which is the base class all reconcile actions inherit (e.g. XHandler.py)

For more detail on the code see the README inside `service-node/packages/reconcile`

This demonstration is adapted from the [code](https://github.com/NSO-developer/vrf-oob-reconcile)
provided by Dan Sullivan through the NSO Developer Hub.  It was initially presented during
[NSO Developer Days 2020](https://www.youtube.com/watch?v=DNwYQGed9TE).

### Dependencies ###

* Python 2.7+ or 3+ based on NSO requirements
* NSO version ~4.7.5
* Cisco IOS NED version ~6.40

### Service Model ###

The service model being reconcilied in this demonstration is a trivial example of a stacked service architecture. The top level service named `circuit-test` does not deploy config directly to the devices. Instead, it configures smaller underlying instances of `service-test` which in-turn know how to apply configuration to a single device. 

### Running the demonstration ###


Clone and cd to the top level directory.

Copy (and extract if necessary) the Cisco IOS NED into the service-node/packages directory.

Type the following from the top level directory.
```
$ make clean all

for i in service-node; do \
	make -C ${i}  clean || exit 1; \
done
make[1]: Entering directory '~/service-reconcile-base/service-node'
ncs --stop >/dev/null 2>&1; true

...

make[1]: Leaving directory '~/service-reconcile-base/service-node/packages/service-test/src'
./mk-init.sh
DEVICE ios_0 CREATED
DEVICE ios_1 CREATED
```

Start the service node and two netsim devices.
```
$ make start

ncs-netsim start
DEVICE ios_0 OK STARTED
DEVICE ios_1 OK STARTED
make app-start

...

sync-result {
    device ios_0
    result true
}
sync-result {
    device ios_1
    result true
}
No modifications to commit.
make[2]: Leaving directory '~/service-reconcile-base/service-node'
make[1]: Leaving directory '~/service-reconcile-base'
```

Load a test service instance and create a deviation on it. This script performs two commits. The first commit creates a new instance of `circuit-test` which creates two new instances of `service-test`. Each of these instances then sets the login banner for a different IOS device using the default value of `HELLO WORLD`.  Then the second commit represents the out-of-band change on the device. We alter the login banner of one of the two IOS devices directly, thus introducing a conflict between the service branch and the device branch of the cdb. 

```
$ source deviation.sh

Commit complete.
Commit complete.
```

Log in to the service node and display the deviation. To see exactly what the differences are, you can use the `re-deploy dry-run` function of a sevice instance to see what changes it would make to the devices branch. In this particular case, you will see the service trying to overwrite the device with the default value with which the service has been configured.
```
$ ncs_cli -u admin
admin connected from 127.0.0.1 using console on DESKTOP-0DK0U96

admin@srv-nso> request services circuit-test TEST re-deploy dry-run
cli {
    local-node {
        data  devices {
                   device ios_0 {
                       config {
                           ios:banner {
              -                login "NEW LOGIN BANNER";
              +                login "HELLO WORLD!";
                           }
                       }
                   }
               }

    }
}
[ok][2020-06-19 10:05:52]
```

Call the reconcile action on the top level service to bring it in-sync with the network.

```
admin@srv-nso> request services reconcile circ-test-reconcile id TEST
status success
[ok][2020-06-19 10:06:02]
admin@srv-nso>
System message at 2020-06-19 10:06:02...
Commit performed by admin via tcp using circuit-test-reconcile[TEST].
```

Run the dry-run to verify that there would be no change to the lower level 
service models with the deploy of the upper level service.   
This has actually already been done in the reconcile action
which is the reason the result of the action was 'success'

```
admin@srv-nso> request services circuit-test TEST re-deploy dry-run
cli {
}
[ok][2020-06-19 10:06:09]
```

Show the final result of service and device configurations

```
admin@srv-nso> show configuration services
circuit-test TEST {
    device1 {
        hostname ios_0;
        banner   "NEW LOGIN BANNER";
    }
    device2 {
        hostname ios_1;
        banner   "HELLO WORLD!";
    }
}
service-test TEST[ios_0] {
    hostname ios_0;
    banner   "NEW LOGIN BANNER";
}
service-test TEST[ios_1] {
    hostname ios_1;
    banner   "HELLO WORLD!";
}
[ok][2020-06-19 10:06:06]

admin@srv-nso> show configuration devices device ios_0 config ios:banner
login "NEW LOGIN BANNER";
[ok][2020-06-19 10:6:50]

admin@srv-nso> show configuration devices device ios_1 config ios:banner
login "HELLO WORLD!";
[ok][2020-06-19 10:7:30]
```

The reconciliation is now complete.  To stop NSO and the netsims, run `make stop`.

### Contribution guidelines ###

Contributions are welcome.  Please follow the Git/GitLab Merge Request process and do an MR to master.

### Who do I talk to? ###

Feel free to mail questions and comments to:

[Scott Barvick @ Data Ductus](mailto:scott.barvick@dataductus.com)

[Caleb Waslter @ Data Ductus](mailto:caleb.wastler@dataductus.com)
