SN_PACKAGES = circuit-test reconcile service-test
NEDS = cisco-ios

all: app-all

neds:
	for i in $(NEDS); do \
		$(MAKE) -C service-node/packages/$${i}/src all || exit 1; \
	done
app-all:
	for p in $(SN_PACKAGES); do\
		make -C service-node/packages/$$p/src all; \
	done
	./mk-init.sh

DIRS=service-node

clean: app-clean dev-clean

app-clean:
	for i in $(DIRS); do \
		$(MAKE) -C $${i}  clean || exit 1; \
	done
	for p in $(SN_PACKAGES); do \
		make -C service-node/packages/$$p/src clean; \
	done
	rm -rf netsim

ned-clean:
	for p in $(NEDS); do \
		make -C service-node/packages/$$p/src clean; \
	done

dev-clean:
	rm -rf service-node/logs; \
	rm -rf service-node/ncs-cdb; \
	rm -rf service-node/state; \
	rm -rf service-node/storedstate \
	rm -rf service-node/target

start stop:
	ncs-netsim $@
	make app-$@

app-start app-stop:
	for i in $(DIRS); do \
		$(MAKE) -C $${i}  $@ || exit 1; \
	done

reset status:
	@for i in $(DIRS); do \
		$(MAKE) -C $${i}  $@ || exit 1; \
	done

cli:
	cd service-node; make cli

#  LocalWords:  SN
