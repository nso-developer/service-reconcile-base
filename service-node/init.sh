#!/bin/sh

set -e

ncs_cli -u admin <<EOF
configure
request devices fetch-ssh-host-keys
request devices sync-from
commit
exit
exit
EOF
