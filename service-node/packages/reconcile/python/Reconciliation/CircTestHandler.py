import ncs.template
from .BaseHandler import BaseHandler

class CircTestHandler(BaseHandler):
    """This class implements the circ-reconcile action"""

    def set_name(self, inputs):
        self.action_name = "circuit-test-reconcile[%s]"%(inputs.id,)
        self.template_name = 'circ-test-reconcile'

    def get_target(self, root, inputs):
        return root.ncs__services.testcirc__circuit_test[inputs.id]

    def reconcile_service(self, rec_action, uid, uid_type='uid'):
        args = rec_action.get_input()
        self.log.info('Argument for reconcile: %s'%(uid))
        setattr(args, uid_type, uid)
        res = rec_action(args)
        self.log.info('Returned from reconcile: %s'%(res.status,))

    def get_variables(self, root, sn, target_serv, inputs, output):
        variables = ncs.template.Variables()
        serv_rec = sn.serv_test_reconcile

        dev1_inst = None
        dev2_inst = None

        # Reconcile the lower lever service instances associated with this top level service.  
        # That way you know the lower-level service is in sync with the device before you read from it
        for test_serv_inst in root.ncs__services.testserv__service_test:
            if test_serv_inst.hostname == target_serv.device1.hostname:
                dev1_inst = test_serv_inst
                self.reconcile_service(serv_rec, test_serv_inst.uid)
            if test_serv_inst.hostname == target_serv.device2.hostname:
                dev2_inst = test_serv_inst
                self.reconcile_service(serv_rec, test_serv_inst.uid)

        # Add the necessary variables to the template
        variables.add('ID', inputs.id)
        variables.add('DEV1_UID', dev1_inst.uid if dev1_inst else None)
        variables.add('DEV2_UID', dev2_inst.uid if dev2_inst else None)
        return variables
