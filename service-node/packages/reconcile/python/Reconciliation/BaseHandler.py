import re
import os
import ncs
import _ncs
from ncs.application import Service
import ncs.maagic as maagic
import ncs.maapi as maapi
from ncs.dp import Action

class BaseHandler(Action):

    action_name = ""
    template_name = ""

    def set_name(self, inputs):
        raise NotImplementedError('ReconcileHandler.set_name')

    def get_target(self, root, inputs):
        raise NotImplementedError('ReconcileHandler.get_target')

    def get_variables(self, root, sn, inputs, output):
        raise NotImplementedError('ReconcileHandler.get_variables')

    @Action.action
    def cb_action(self, uinfo, name, kp, inputs, output):
        self.set_name(inputs)
        sni = self.get_action_name()

        self.log.info('%s: START' % (self.get_action_name(),))
        output.status = 'success'

        port = int(os.getenv('NCS_IPC_PORT', _ncs.NCS_PORT))
        self.log.info('Using NCS IPC port value [%d]' % (port))

        try:
            with maapi.Maapi('127.0.0.1', port, path=None) as self.maapi:
                self.maapi.start_user_session('admin', self.get_action_name(), [])
                with self.maapi.start_write_trans(db=ncs.RUNNING) as t:
                    root = maagic.get_root(t, shared=False)
                    sn = root.ncs__services.reconcile__reconcile

                    target_serv = self.get_target(root, inputs)
                    template = ncs.template.Template(sn)
                    variables = self.get_variables(root, sn, target_serv, inputs, output)
                    template.apply(self.template_name, variables)

                    self.validate_output(root, target_serv, output)

                    if output.status != "failed":
                        self.log.info(
                            '%s:Saving/Applying reconcile changes' % (self.get_action_name()))
                        t.apply()
        except Exception as e:
            output.status = 'failed'
            output.error_message = 'Failure during reconcillation processing'
            self.log.error(e)
        return ncs.CONFD_OK

    def get_action_name(self):
        return self.action_name

    def validate_output(self, root, target_serv, output):
        # Execute dry run to check if the reconcile was successful
        dryRun = root.ncs__services.commit_dry_run
        inp = dryRun.get_input()
        result = dryRun(inp)
        if result.cli and result.cli.local_node.data:
            # Any device/network changes mean changes outside of what we can reconile
            self.log.info('%s dry-run results: [%s]' % (self.get_action_name(), result.cli.local_node.data))
            m = re.search(r'^\s*services\s+{\n', result.cli.local_node.data, re.MULTILINE)
            data = result.cli.local_node.data[:m.start()] if m else result.cli.local_node.data

            if re.search(r'\s+[\+\-]\s+.*\n', data):
                self.log.info('%s FOUND CHANGES: [%s]' % (self.get_action_name(), data))
                self.log.error(
                    (
                        '%s failed because of changes to the network device\n'
                        'that cannot be automatically reconciled with the dry-run appended:'
                    ) % (self.get_action_name(),)
                )
                output.status = 'failed'
                output.error_message = result.cli.local_node.data
        else:
            # No output from commit dry-run, we need to check re-deploy to make
            # sure there aren't any unreconcilable changes in the CDB
            self.log.info('%s starting re-deploy dry-run' % (self.get_action_name()))

            redeploy = target_serv.re_deploy
            inp = redeploy.get_input()
            inp.dry_run.create()
            result = redeploy(inp)
            if (result.cli) and (result.cli.local_node.data):
                self.log.info((
                        '%s failed to reconcile all deviations between device and service\n'
                        'remaining changes: [%s]'
                    ) % (self.get_action_name(), result.cli.local_node.data)
                )
                output.status = 'failed'
                output.error_message = result.cli.local_node.data
