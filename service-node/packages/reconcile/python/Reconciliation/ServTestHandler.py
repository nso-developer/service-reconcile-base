import ncs.template
from .BaseHandler import BaseHandler

class ServTestHandler(BaseHandler):
    """This class implements the serv-reconcile action"""

    def set_name(self, inputs):
        self.action_name = 'service-test-reconcile[%s]'%(inputs.uid,)
        self.template_name = 'serv-test-reconcile'

    def get_target(self, root, inputs):
        return root.ncs__services.testserv__service_test[inputs.uid]

    def get_variables(self, root, sn, target_serv, inputs, output):
        variables = ncs.template.Variables()
        variables.add('UID', inputs.uid)
        return variables
