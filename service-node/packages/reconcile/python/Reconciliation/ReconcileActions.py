'''
    Register all reconcile actions for all services
'''
import ncs
import _ncs

from .ServTestHandler import ServTestHandler
from .CircTestHandler import CircTestHandler

########################################################
# NSO Action Registration
########################################################

class ReconcileActions(ncs.application.Application):
    '''Class for registering the reconcile actions'''
    def setup(self):

        self.log.info('Reconcile Action Register Starting...')
        self.register_action('serv-test-reconcile-point', ServTestHandler, [])
        self.register_action('circ-test-reconcile-point', CircTestHandler, [])
        self.log.info('Reconcile Action Register Completed...')

    def teardown(self):
        self.log.info('Action FINISHED')
