# Reconcile Package

This NSO package contains two NSO actions for reconciling a stacked service model. Originally inspired by [Dan Sullivan](https://github.com/NSO-developer/vrf-oob-reconcile), this package uses templates to reverse map changes in the network back into the NSO service model. This enables the NSO service model to be brought into sync with the network without creating potentially service-effecting changes on the device. 

Fundamental decisions based on the number of services to reconcile include:

    - Minimizing the amount of code necessary to create a new reconcile action.
    - Centralizing the code for all service reconciliation into a single package.

These choices allow you to focus your development directly on the template logic needed to do the actual reconciliation and improve your ability to scale your solution. By centralizing the code into a single package, only one package needs to run a python VM rather than the number of VMs scaling linearly with the number of reconcile actions. 

## Python/Reconciliation/BaseHandler.py

This file contains the core logic, abstracted from Dan's example, which drives the process of reconciling a single instance of a service. The order of operations for a given reconcile action is as follows:

    1. Set the name of the action and the reverse mapping template that are being used.

    2. Create the necessary variables for the reverse mapping template. This is usually
       the uid of the target service.

    3. Apply the reverse mapping template which will update the service data.

    4. Determine if the reconciliation was successful by using `commit dry-run` to make sure
       no changes will be made to the device after the service is updated and created.

    5. Commit or abort the transaction based on the result of step 4.

To implement a reconcile action using this code, the inheriting class must implement an interface of 3 methods: 

    - `set_name` sets the name of the reverse mapping template to be called and the name
       of the action used in the logs.

    - `get_target` returns the maagic node representing the service to be reconciled.

    - `get_variables` returns an NSO template variable object containing the necessary values
       for the reverse mapping template.
