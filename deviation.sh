#!/bin/bash

# to reconcile the deviation run 
# request services reconcile circ-test-reconcile id TEST

ncs_cli -u admin << EOF
conf
set services circuit-test TEST device1 hostname ios_0
set services circuit-test TEST device2 hostname ios_1
commit

set devices device ios_0 config ios:banner login "NEW LOGIN BANNER"
commit
EOF
